# html-validate-angular changelog

### [3.0.2](https://gitlab.com/html-validate/html-validate-angular/compare/v3.0.1...v3.0.2) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([0cd5d8b](https://gitlab.com/html-validate/html-validate-angular/commit/0cd5d8be5d5dcca5d9273d207f570985a345a2c7))

### [3.0.1](https://gitlab.com/html-validate/html-validate-angular/compare/v3.0.0...v3.0.1) (2021-06-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([c3cdc70](https://gitlab.com/html-validate/html-validate-angular/commit/c3cdc706e2bdccc229858ec74ad736f444898869))

## [3.0.0](https://gitlab.com/html-validate/html-validate-angular/compare/v2.9.0...v3.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([0c6b846](https://gitlab.com/html-validate/html-validate-angular/commit/0c6b846eccc6a41e29779cf0daaebfe53d12ef37))

## [2.9.0](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.6...v2.9.0) (2020-11-08)

### Features

- `html-validate@4.0.0` compatibility ([0a39d09](https://gitlab.com/html-validate/html-validate-angular/commit/0a39d09b583b48e754e2c4626892757920c9b824))

### Bug Fixes

- bump `peerDependency` and `engines` requirements ([9d5c153](https://gitlab.com/html-validate/html-validate-angular/commit/9d5c15316778cf7341c9d4f1ea573238e9361094))
- migrate to `dist` folder ([4df2e56](https://gitlab.com/html-validate/html-validate-angular/commit/4df2e56293c64e70b67b8214941dd06f0b0d2c47))

## [2.8.6](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.5...v2.8.6) (2020-11-01)

## [2.8.5](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.4...v2.8.5) (2020-10-25)

## [2.8.4](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.3...v2.8.4) (2020-04-20)

### Bug Fixes

- **transform:** strip templating from source ([591c2ef](https://gitlab.com/html-validate/html-validate-angular/commit/591c2eff729b11cdbcca55ac2386af32d6012abb)), closes [#4](https://gitlab.com/html-validate/html-validate-angular/issues/4)

## [2.8.3](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.2...v2.8.3) (2020-03-29)

## [2.8.2](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.1...v2.8.2) (2020-02-08)

## [2.8.1](https://gitlab.com/html-validate/html-validate-angular/compare/v2.8.0...v2.8.1) (2020-01-26)

### Bug Fixes

- **deps:** bump html-validate to 2.11.0 ([83aed39](https://gitlab.com/html-validate/html-validate-angular/commit/83aed39c6b0181d5a70c80b2b8e8445b4081ab73))

# [2.8.0](https://gitlab.com/html-validate/html-validate-angular/compare/v2.7.0...v2.8.0) (2019-12-05)

### Features

- handle source `offset` ([d9c11de](https://gitlab.com/html-validate/html-validate-angular/commit/d9c11dedf012f5139cac2e06247b2046c07edc12))

# [2.7.0](https://gitlab.com/html-validate/html-validate-angular/compare/v2.6.3...v2.7.0) (2019-11-17)

### Features

- html-validate@2.0.0 compatibility ([baa4e1a](https://gitlab.com/html-validate/html-validate-angular/commit/baa4e1adbad9865b11bce95ef6c0850f2c2e8692))

## [2.6.3](https://gitlab.com/html-validate/html-validate-angular/compare/v2.6.2...v2.6.3) (2019-08-19)

### Bug Fixes

- **attribute:** handle ng-src with interpolation ([b268b9d](https://gitlab.com/html-validate/html-validate-angular/commit/b268b9d)), closes [#2](https://gitlab.com/html-validate/html-validate-angular/issues/2)

## 2.6.2 (2019-03-27)

- handle `ng-transclude`, `ng-bind-template` and `ng-pluralize` as dynamic content.

## 2.6.1 (2019-03-26)

- `html-validate@0.24` compatibility.

## 2.6.0 (2019-03-20)

- Implement `processElement` hook adding support for dynamic text content from
  `ng-bind`, `ng-bind-html` and `translate`.

## 2.5.0 (2019-02-24)

- Handles `ng-class`, `ng-required` etc.
- Improved attribute handling.
- Rewritten in typescript

## 2.4.0 (2019-02-17)

- Bump html-validate to 0.21.0
- Bump deps.

## 2.3.0 (2019-01-29)

- Bump html-validate to 0.20.0
- Fix crash when using arrow functions as template.

## 2.2.0 (2019-01-27)

- Bump html-validate to 0.19.0
- Fix crash when using boolean attributes.

## 2.1.0 (2019-01-10)

- Process regular html files as well as js to handle interpolation.
- Process interpolated attributes to mark dynamic values.

## 2.0.3 (2018-12-16)

- Bump html-validate to 0.16.1

## 2.0.2 (2018-11-07)

- Bump html-validate to 0.14.2
