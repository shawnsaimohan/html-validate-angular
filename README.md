# html-validate-angular

[![pipeline status](https://gitlab.com/html-validate/html-validate-angular/badges/master/pipeline.svg)](https://gitlab.com/html-validate/html-validate-angular/commits/master)
[![coverage report](https://gitlab.com/html-validate/html-validate-angular/badges/master/coverage.svg)](https://gitlab.com/html-validate/html-validate-angular/commits/master)

AngularJS support for [html-validate].

- Extracts templates from components and routes with inline templates.
- Transforms interpolated attributes (including `ng-attr-*`) in HTML.
- Handles dynamic bindings for rules checking presence of text.

Typescript is not yet supported. [Help wanted](https://gitlab.com/html-validate/html-validate/-/issues/94)

[html-validate]: https://www.npmjs.com/package/html-validate

## Example

```js
export const FooComponent = {
  template: "<button>foo</button>",
};
```

```js
export function routeConfig($routeProvider) {
  $routeProvider.when("/route", { template: "<p>foo</i>" });
}
```

In both cases it will allow html-validate to parse and detect errors in the
templates:

    component.js
      2:13  error  Button is missing type attribute  button-type

    route.js
      2:51  error  Mismatched close-tag, expected '</p>' but found '</i>'  close-order

## Usage

    npm install --save-dev html-validate-angular

In `.htmlvalidate.json`:

```js
{
  "transform": {
    "^.*\\.js$": "html-validate-angular/js",
    "^.*\\.html$": "html-validate-angular/html"
  }
}
```

HTML processing is optional but is needed when attribute interpolation is used.
