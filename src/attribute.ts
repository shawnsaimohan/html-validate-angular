import { AttributeData, DynamicValue } from "html-validate";

const directives = [
	"checked",
	"class",
	"disabled",
	"href",
	"maxlength",
	"minlength",
	"pattern",
	"readonly",
	"required",
	"selected",
	"src",
	"style",
	"value",
];

/* eslint-disable-next-line security/detect-non-literal-regexp */
const matchNgAttr = new RegExp(`^ng-(?:attr-(.*)|(${directives.join("|")}))$`);

/**
 * Handle ng-xyz and ng-attr-xyz attributes.
 *
 * It yields both the original attribute (as-is) and an aliased attribute
 * xyz. E.g. `ng-src="{{ foo }}"` yields both:
 *
 * - `ng-src="{{ foo }}"`
 * - `src="DynamicValue"`
 */
function* processNgAttr(attr: AttributeData, key: string): IterableIterator<AttributeData> {
	/* passthru original attribute */
	yield attr;

	/* setup ng-attr-x alias */
	yield Object.assign({}, attr, {
		key,
		value: new DynamicValue(attr.value as string),
		originalAttribute: attr.key,
	});
}

export function* processAttribute(attr: AttributeData): IterableIterator<AttributeData> {
	const ngAttr = attr.key.match(matchNgAttr);
	if (ngAttr) {
		yield* processNgAttr(attr, ngAttr[1] || ngAttr[2]);
		return;
	}

	/* test if attribute contains interpolation */
	if (typeof attr.value === "string" && attr.value.match(/{{.*}}/)) {
		yield Object.assign({}, attr, {
			value: new DynamicValue(attr.value),
		});
		return;
	}

	/* passthru original attribute */
	yield attr;
}
