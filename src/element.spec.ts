import { HtmlElement, Location, NodeClosed, TextNode } from "html-validate";
import { processElement } from "./element";

const location: Location = {
	filename: "mock-filename",
	line: 1,
	column: 1,
	offset: 0,
	size: 1,
};

it("should add dynamic text when ng-bind is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("ng-bind", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-bind-html is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("ng-bind-html", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-bind-template is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("ng-bind-template", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when translate is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("translate", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-transclude is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("ng-transclude", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <ng-transclude>", () => {
	expect.assertions(1);
	const node = new HtmlElement("ng-transclude", null, NodeClosed.EndTag, null, location);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-pluralize is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("ng-pluralize", "bar", null, null);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <ng-pluralize>", () => {
	expect.assertions(1);
	const node = new HtmlElement("ng-pluralize", null, NodeClosed.EndTag, null, location);
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should not add dynamic text if no dynamic content attribute is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	processElement(node);
	expect(node.childNodes).toEqual([]);
});
