import { Config, Parser, Source, HtmlElement, DynamicValue } from "html-validate";
import { transformFile, transformString } from "html-validate/test-utils";
import { processAttribute } from "./attribute";
import { angularTransformJS, angularTransformHTML } from "./transform";

it("should extract template from components", () => {
	expect.assertions(2);
	const result = transformFile(angularTransformJS, "./test/component.js");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 12,
		  "data": "<p>foo</i>",
		  "filename": "./test/component.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 2,
		  "offset": 42,
		  "originalData": "<p>foo</i>",
		}
	`);
});

it("should extract templates from routes", () => {
	expect.assertions(4);
	const result = transformFile(angularTransformJS, "./test/route.js");
	expect(result).toHaveLength(3);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 31,
		  "data": "<p>foo</i>",
		  "filename": "./test/route.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 3,
		  "offset": 93,
		  "originalData": "<p>foo</i>",
		}
	`);
	expect(result[1]).toMatchInlineSnapshot(`
		Object {
		  "column": 14,
		  "data": "
						<button>
							bar
						</button>
					",
		  "filename": "./test/route.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 5,
		  "offset": 137,
		  "originalData": "
						<button>
							bar
						</button>
					",
		}
	`);
	expect(result[2]).toMatchInlineSnapshot(`
		Object {
		  "column": 14,
		  "data": "<button type=\\"{{     }}\\">text</button>",
		  "filename": "./test/route.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 12,
		  "offset": 226,
		  "originalData": "<button type=\\"{{valid}}\\">text</button>",
		}
	`);
});

it("should extract template from html file", () => {
	expect.assertions(2);
	const result = transformFile(angularTransformHTML, "./test/template.html");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 1,
		  "data": "<div>
			<button>text</button>
			<button type=\\"invalid\\">text</button>
			<button type=\\"{{       }}\\">text</button>
			<button ng-attr-type=\\"valid\\">text</button>
		</div>

		<!-- should detect duplicated ng-class but not duplicate class + ng-class -->
		<p class=\\"foo\\" ng-class=\\"bar\\"></p>
		<p ng-class=\\"foo\\" ng-class=\\"bar\\"></p>

		<!-- should detect ng-bind and friends as having textual content -->
		<h1></h1>
		<h2 ng-bind=\\"foo\\"></h2>
		<h3 ng-bind-html=\\"bar\\"></h3>
		<h3 ng-bind-template=\\"baz\\"></h3>
		<h4 translate=\\"KEY\\"></h4>

		<h1 ng-transclude></h1>
		<h1><ng-transclude></ng-transclude></h1>
		<h1 ng-pluralize></h1>
		<h1><ng-pluralize></ng-pluralize></h1>

		<!-- should not detect errors inside {{     }}  -->
		<div>{{        }}</div>
		<div>{{                               }}</div>
		",
		  "filename": "./test/template.html",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 0,
		  "originalData": "<div>
			<button>text</button>
			<button type=\\"invalid\\">text</button>
			<button type=\\"{{ valid }}\\">text</button>
			<button ng-attr-type=\\"valid\\">text</button>
		</div>

		<!-- should detect duplicated ng-class but not duplicate class + ng-class -->
		<p class=\\"foo\\" ng-class=\\"bar\\"></p>
		<p ng-class=\\"foo\\" ng-class=\\"bar\\"></p>

		<!-- should detect ng-bind and friends as having textual content -->
		<h1></h1>
		<h2 ng-bind=\\"foo\\"></h2>
		<h3 ng-bind-html=\\"bar\\"></h3>
		<h3 ng-bind-template=\\"baz\\"></h3>
		<h4 translate=\\"KEY\\"></h4>

		<h1 ng-transclude></h1>
		<h1><ng-transclude></ng-transclude></h1>
		<h1 ng-pluralize></h1>
		<h1><ng-pluralize></ng-pluralize></h1>

		<!-- should not detect errors inside {{ ... }}  -->
		<div>{{ \\"</p>\\" }}</div>
		<div>{{ foo && bar ? 'true' : 'false' }}</div>
		",
		}
	`);
});

it("querySelector should find class with ng-class present", () => {
	expect.assertions(3);
	const parser = new Parser(Config.empty().resolve());
	const source: Source = {
		data: '<p class="foo" ng-class="bar"></p>',
		filename: "inline",
		line: 1,
		column: 1,
		offset: 0,
		hooks: {
			processAttribute,
		},
	};
	const doc = parser.parseHtml(source);
	const node = doc.querySelector(".foo");
	expect(node).toBeInstanceOf(HtmlElement);
	expect(Array.from(node.classList)).toEqual(["foo"]);
	expect(node.getAttribute("class", true)).toEqual([
		expect.objectContaining({
			key: "class",
			value: "foo",
		}),
		expect.objectContaining({
			key: "class",
			value: expect.any(DynamicValue),
			originalAttribute: "ng-class",
		}),
	]);
});

it("should strip templating from html", () => {
	expect.assertions(2);
	const result = transformString(angularTransformHTML, "<p>{{ foobar }}</p>");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 1,
		  "data": "<p>{{        }}</p>",
		  "filename": "inline",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 0,
		  "originalData": "<p>{{ foobar }}</p>",
		}
	`);
});

it("should strip templating from js", () => {
	expect.assertions(2);
	const result = transformString(
		angularTransformJS,
		"const c = { template: '<p>{{ foobar }}</p>' };"
	);
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 23,
		  "data": "<p>{{        }}</p>",
		  "filename": "inline",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 23,
		  "originalData": "<p>{{ foobar }}</p>",
		}
	`);
});
