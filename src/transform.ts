import { Source, TemplateExtractor, compatibilityCheck } from "html-validate";
import { processAttribute } from "./attribute";
import { processElement } from "./element";
import { stripTemplating } from "./utils";

/* eslint-disable-next-line @typescript-eslint/no-var-requires */
const pkg = require("../package.json");

/* warn when using unsupported html-validate library version */
/* istanbul ignore next */
if (compatibilityCheck) {
	const range = pkg.peerDependencies["html-validate"];
	compatibilityCheck(pkg.name, range);
}

function transform(source: Source): Source {
	return {
		...source,
		data: stripTemplating(source.data),
		originalData: source.data,
		hooks: {
			processAttribute,
			processElement,
		},
	};
}

export function angularTransformJS(source: Source): Iterable<Source> {
	const te = TemplateExtractor.fromString(source.data, source.filename);
	const sources = te.extractObjectProperty("template");
	return sources.map(transform);
}

export function angularTransformHTML(source: Source): Iterable<Source> {
	return [transform(source)];
}

angularTransformJS.api = 1;
angularTransformHTML.api = 1;
