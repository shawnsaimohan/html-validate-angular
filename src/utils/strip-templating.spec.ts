import { stripTemplating } from "./strip-templating";

it("should replace {{ .. }} with whitespace", () => {
	expect.assertions(1);
	expect(stripTemplating("foo {{ bar }} baz")).toBe("foo {{     }} baz");
});

it("should replace all instances", () => {
	expect.assertions(1);
	expect(stripTemplating("{{ a }} {{ b }}")).toBe("{{   }} {{   }}");
});
