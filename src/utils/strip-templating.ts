/**
 * Strip anything inside `{{ .. }}` in case it contains anything that looks like
 * markup, i.e. it will be ignored
 */
export function stripTemplating(str: string): string {
	return str.replace(/{{(.*?)}}/g, (_, m) => {
		const filler = " ".repeat(m.length);
		return `{{${filler}}}`;
	});
}
