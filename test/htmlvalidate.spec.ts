import { ConfigData, HtmlValidate, Report } from "html-validate";
import { angularTransformJS, angularTransformHTML } from "../src/transform";

jest.mock("transform-js", () => angularTransformJS, { virtual: true });
jest.mock("transform-html", () => angularTransformHTML, { virtual: true });

const config: ConfigData = {
	root: true,
	extends: ["html-validate:recommended"],

	transform: {
		"\\.js$": "transform-js",
		"\\.html$": "transform-html",
	},
};

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
			const src: any = msg;
			delete src.ruleUrl;
			delete src.context;
		}
	}
}

it('should find errors in "component.js"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/component.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "route.js"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/route.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "route-params.js"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/route-params.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "template.html"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/template.html");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('"regressions.html"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate({
		...config,
		rules: {
			"void-style": ["error", { style: "selfclose" }],
		},
	});
	const report = htmlvalidate.validateFile("test/regressions.html");
	filterReport(report);
	expect(report.results).toMatchSnapshot();
	expect(report.valid).toBeTruthy();
});
