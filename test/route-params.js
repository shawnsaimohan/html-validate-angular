export function routeConfig($routeProvider) {
	$routeProvider
		.when("/foo/arrow-1/:id", {
			template: (param) => `<my-component binding="${param.id}"></my-component-error>`,
		})
		.when("/foo/arrow-2/:id", {
			template: (param) => {
				return `<my-component binding="${param.id}"></my-component-error>`;
			},
		})
		.when("/foo/arrow-3", {
			template: () => "<my-component></my-component-error>",
		})
		.when("/foo/function/:id", {
			template: function (param) {
				return `<my-component binding="${param.id}"></my-component-error>`;
			},
		});
}
