export function routeConfig($routeProvider) {
	$routeProvider
		.when("/route", { template: "<p>foo</i>" })
		.when("/", {
			template: `
				<button>
					bar
				</button>
			`,
		})
		.when("/dynamic-type", {
			template: `<button type="{{valid}}">text</button>`,
		});
}
